package pl.hejman.logpic.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Arrays;
import java.util.Random;

/**
 * This class is responsible for reading the XML file and mapping it
 * to new logical picture ready to be solved
 */
public class XMLGameReader {

    private FieldState[][] loadedGame;
    private File fileWithLevel;
    private int amountOfRowsInLoadedGame;
    private int amountofColumnsInLoadedGame;

    public XMLGameReader() {
        this.loadedGame = loadGameFromXML();
        this.fileWithLevel =getRandomFileWithLevel();
    }

    public FieldState[][] getLoadedGame() {
        return this.loadedGame;
    }

    /**
     * This class get the random file with levels
     * from levels/ directory
     * @return random file with level
     */
    public File getRandomFileWithLevel() {
        File directoryWithLevel = new File("levels/");
        File[] listOfLevels = directoryWithLevel.listFiles();
        int randomIndex = new Random().nextInt(listOfLevels.length);
        return listOfLevels[randomIndex];
    }

    /**
     * This is the primary method of this class - it creates the game
     * @return arays of Fieldstates as a ready game
     */
    public FieldState[][] loadGameFromXML() {
        FieldState[][] game = new FieldState[9][9];
        for (FieldState[] row : game) {
            Arrays.fill(row, FieldState.EMPTY);
        }
        try {
            setAmountofColumnsInLoadedGame(9);
            setAmountOfRowsInLoadedGame(9);
            File fXmlFile = getRandomFileWithLevel();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("rows");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    getNumbersFromNode(game, (Element) nNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return game;
    }

    /**
     * This method simply gets numbers to be set on number panels from given game
     * @param game
     * @param nNode
     */
    private void getNumbersFromNode(FieldState[][] game, Element nNode) {
        Element eElement = nNode;
        for(int i=1;i<amountOfRowsInLoadedGame + 1;i++) {
            String rowNumber ="row" + i;
            int rowNumberInArray = i-1;
            String parsedRow =getXMLTagValue(rowNumber , eElement);
            fillSolutionRow(game, rowNumberInArray, parsedRow);
        }
    }

    /**
     * This method fills the solution picture row by row
     * @param game
     * @param rowNumberInArray
     * @param parsedRow
     */
    private void fillSolutionRow(FieldState[][] game, int rowNumberInArray, String parsedRow) {
        for(int j=0;j<amountofColumnsInLoadedGame;j++) {
            Character field = parsedRow.charAt(j);
            if(field == 'X') {
                game[rowNumberInArray][j] = FieldState.PAINTED;
            }
            else {
                game[rowNumberInArray][j] = FieldState.EMPTY;
            }
        }
    }

    /**
     * This is helpful method which parses Tag Value of a XML file
     * @param sTag
     * @param eElement
     * @return
     */
    private static String getXMLTagValue (String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    public void setAmountOfRowsInLoadedGame(int amountOfRowsInLoadedGame) {
        this.amountOfRowsInLoadedGame = amountOfRowsInLoadedGame;
    }

    public void setAmountofColumnsInLoadedGame(int amountofColumnsInLoadedGame) {
        this.amountofColumnsInLoadedGame = amountofColumnsInLoadedGame;
    }

    public int getAmountOfRowsInLoadedGame() {
        return amountOfRowsInLoadedGame;
    }

    public int getAmountofColumnsInLoadedGame() {
        return amountofColumnsInLoadedGame;
    }
}
