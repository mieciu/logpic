package pl.hejman.logpic.model;

/**
 *  This enumeration is used in order to inform observers what to update
 */
public enum UpdateAction {
        NEW_GAME,
        CHECK,
        SELECTED_STATE
}
