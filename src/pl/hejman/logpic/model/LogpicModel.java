package pl.hejman.logpic.model;

import java.util.Arrays;
import java.util.Observable;

/**
*   This is the main model class - it represents logical picture,
*   which the player is solving.
*/
public class LogpicModel extends Observable {

     /* Amount of rows in the picure */
     private int amountOfRows;
     /* Amount of columns in the picture */
     private int amountOfColumns;
     /* Solution of the picture */
     private FieldState[][] solution;
     /* Generated gameWithUserInputs with user inputs */
     private FieldState[][] gameWithUserInputs;
     /* State of field selected by user */
     private FieldState selectedState;

    public LogpicModel() {
        createNewGame();
    }

    /** This method creates new game */
    public void createNewGame() {
        solution = generateSolution();
        gameWithUserInputs = generateEmptyGame(getAmountOfRows(),getAmountOfColumns());
    }

    /**
     * This method set FieldState chosen by player
     * @param selectedState
     */
    public void setSelectedState(final FieldState selectedState) {
        this.selectedState = selectedState;
    }

    /** This method returns Fieldstate selected by player */
    public FieldState getSelectedState() {
        return this.selectedState;
    }

    /**
     * This method sets the FieldState on certain field
     * @param x   coordinate X of fields array
     * @param y   coordinate Y of fields array
     * @param fieldState  a value to be set
     */
    public void setFieldState(final int x, final int y, final FieldState fieldState) {
        gameWithUserInputs[x][y] = fieldState;
    }

    /**
     * This method returns Fieldstate put by user on certain field
     * @param x   coordinate X of fields array
     * @param y   coordinate Y of fields array
     * @return
     */
    public FieldState getFieldState(final int x, final int y) {
        return gameWithUserInputs[x][y];
    }

    /**
     * This method returns the fieldstate form the solution on certain field
     * @param x  coordinate X of fields array
     * @param y  coordinate Y of fields array
     * @return
     */
    public FieldState getSolutionState(final int x, final int y) {
        return solution[x][y];
    }

    /**
     * This method returns test result whether the single fieldstate is set properly
     * @param x coordinate X of fields array
     * @param y coordinate Y of fields array
     * @return
     */
    public boolean isProperlySetByUserAtField(final int x, final int y) {
        if (gameWithUserInputs[x][y].equals(FieldState.MARKED)) {
            if (solution[x][y].equals(FieldState.PAINTED))
                return false;
            else
                return true;
        }
        else
            return gameWithUserInputs[x][y].equals(solution[x][y]);
    }

    /** This method checks if the current game is won */
    public boolean isCurrentGameWon() {
        for(int i=0;i<getAmountOfRows();i++) {
            for(int j=0;j<getAmountOfColumns();j++) {
                if(!isProperlySetByUserAtField(i, j))
                    return false;
            }
        }
        return true;
    }

    /** This method loads the solution of current game */
    private FieldState[][] generateSolution() {
        XMLGameReader xmlGameReader = new XMLGameReader();
        FieldState[][] loadedGame = xmlGameReader.getLoadedGame();
        setPicureSizeFromLoadedGame(xmlGameReader);
        return loadedGame;
    }

    /**
     *  This method sets Picture size according to loaded game
     * @param xmlGameReader
     */
    private void setPicureSizeFromLoadedGame(final XMLGameReader xmlGameReader) {
        setAmountOfRows(xmlGameReader.getAmountOfRowsInLoadedGame());
        setAmountOfColumns(xmlGameReader.getAmountofColumnsInLoadedGame());
    }

    /**
     * This method generates empty game for user to draw on
     * @param amountOfRows
     * @param amountOfColumns
     * @return
     */
    private FieldState[][] generateEmptyGame(final int amountOfRows, final int amountOfColumns) {
        FieldState[][] game = new FieldState[amountOfRows][amountOfColumns];
        for (FieldState[] row : game) {
            Arrays.fill(row, FieldState.EMPTY);
        }
        return game;
    }

    public void setChanged() {
        super.setChanged();
    }

    public int getAmountOfRows() {
        return amountOfRows;
    }

    public void setAmountOfRows(final int amountOfRows) {
        this.amountOfRows = amountOfRows;
    }

    public int getAmountOfColumns() {
        return amountOfColumns;
    }

    public void setAmountOfColumns(final int amountOfColumns) {
        this.amountOfColumns = amountOfColumns;
    }
}
