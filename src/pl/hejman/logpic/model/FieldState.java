package pl.hejman.logpic.model;

/**
 *  This enumeration is used to represent the state of a single field of a picture
 */
public enum FieldState {
        EMPTY,   /** Blank field **/
        PAINTED, /** Black, painted field **/
        MARKED   /** Marked with "X" field **/
}

