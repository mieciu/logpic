package pl.hejman.logpic.view;

import pl.hejman.logpic.model.FieldState;

import javax.swing.*;
import java.awt.*;

/**
 * This class represents the instance of a single field in this game
 */
public class Field extends JLabel {

    /* X and Y coordinates of a single field instance of PicturePanel */
    private int x;
    private int y;

    public Field(int x, int y) {
        super("", CENTER);
        this.x = x;
        this.y = y;
        setPreferredSize(new Dimension(25, 25));
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
        setFont(new Font(Font.DIALOG, Font.PLAIN, 20));
        setOpaque(true);
    }

    /**
     * This method sets the single Field's appearance according to it's state
     * @param state
     */
    public void setAppearanceAccordingToState(final FieldState state){
        switch (state) {
            case PAINTED:
                this.setBackground(Color.BLACK);
                this.setText("");
                break;
            case MARKED:
                this.setBackground(Color.WHITE);
                this.setText("X");
                break;
            case EMPTY:
                this.setBackground(Color.WHITE);
                this.setText("");
                break;
            default:
                throw new IllegalArgumentException();
        }

    }

    /**
     * This method returns fields X coordinate on the picture
     * @return
     */
    public int getCoordinateX(){
        return this.x;
    }

    /**
     * This method returns fields Y coordinate on the picture
     * @return
     */
    public int getCoordinateY(){
        return this.y;
    }


}
