package pl.hejman.logpic.view;

import javax.swing.*;

/**
 *  This class represents the pop-up window with Game rules
 *
 */
public class PopUpWithHelp extends JFrame {

    private final String text = "New Game - pick a new, random game \n" +
                                "Check - check your solution \n" +
                                "Help - display the help message\n" +
                                "Exit - Quit the program\n" +
                                "\n" +
                                "\n" +
                                "Rules are pretty simple - you have to draw a picture\n" +
                                "according to given numbers. Each number represents group\n" +
                                "of painted close together fields which should appear in a row/column\n" +
                                "separated by at least one empty field\n\n" +
                                "You can pick action to perform on a field - either Paint or Clear it.\n" +
                                "You can Mark fields which you are sure that they shouldn't be painted";

    /**
     *  This method displays the window
     */
    public void display(){
        JOptionPane.showMessageDialog(null, getText(), null, JOptionPane.INFORMATION_MESSAGE, null);
    }

    /**
     *  This method returns text to be shown in window
     */
    private String getText() {
        return text;
    }

}
