package pl.hejman.logpic.view;

import pl.hejman.logpic.controller.ButtonPanelController;
import pl.hejman.logpic.controller.PicturePanelController;
import pl.hejman.logpic.model.LogpicModel;

import javax.swing.*;
import java.awt.*;


/**
 *   Main View Class - it initalizes the most of components
 */
public class MainLogpicPanel extends JFrame {

    public MainLogpicPanel(LogpicModel logpicModel) {
        super("Logpic");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        logpicModel.createNewGame();
        prepareAndAddPicturePanel(gridBagConstraints, logpicModel);
        prepareAndAddMockPanel(gridBagConstraints);
        prepareAndAddUpperNumberPanel(gridBagConstraints, logpicModel);
        prepareAndAddSideNumberPanel(gridBagConstraints, logpicModel);
        prepareAndAddButtonPanel(gridBagConstraints, logpicModel);
        preparePopUpWindowAfterChecking(logpicModel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Prepare and add the panel with buttons to gridbag
     * @param gridBagConstraints
     * @param logpicModel
     */
    private void prepareAndAddButtonPanel(final GridBagConstraints gridBagConstraints, final LogpicModel logpicModel) {
        ButtonPanel buttonPanel = new ButtonPanel();
        ButtonPanelController buttonPanelController = new ButtonPanelController(logpicModel);
        buttonPanel.addControllerToTheButtons(buttonPanelController);
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipady = 40;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridx = 0;
        add(buttonPanel,gridBagConstraints);
        logpicModel.addObserver(buttonPanel);
    }

    /**
     * Prepare and add the windows, which will be displayed after checking
     * @param logpicModel
     */
    private void preparePopUpWindowAfterChecking(final LogpicModel logpicModel) {
        PopUpAfterChecking popup = new PopUpAfterChecking();
        logpicModel.addObserver(popup);
    }

    /**
     * Prepare and add panel with the picture
     * @param gridBagConstraints
     * @param logpicModel
     */
    private void prepareAndAddPicturePanel (final GridBagConstraints gridBagConstraints, final LogpicModel logpicModel) {
        PicturePanel picturePanel = new PicturePanel();
        PicturePanelController picturePanelController = new PicturePanelController(logpicModel);
        picturePanel.setGame(logpicModel);
        picturePanel.setController(picturePanelController);
        gridBagConstraints.fill =GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridx = 1;
        add(picturePanel,gridBagConstraints);
        logpicModel.addObserver(picturePanel);
    }

    /**
     * Prepare and add the panel with numbers on the side of picture
     * @param gridBagConstraints
     * @param logpicModel
     */
    private void prepareAndAddSideNumberPanel (final GridBagConstraints gridBagConstraints, final LogpicModel logpicModel) {

        SideNumberPanel sidePanel = new SideNumberPanel();
        sidePanel.setGame(logpicModel);
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridx = 0;
        add(sidePanel,gridBagConstraints);
        logpicModel.addObserver(sidePanel);
    }

    /**
     * Prepare and add the panel with numbers above the picture
     * @param gridBagConstraints
     * @param logpicModel
     */
    private void prepareAndAddUpperNumberPanel(final GridBagConstraints gridBagConstraints, final LogpicModel logpicModel) {

        UpperNumberPanel upperPanel=new UpperNumberPanel();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 1;
        add(upperPanel,gridBagConstraints);
        upperPanel.setGame(logpicModel);
        logpicModel.addObserver(upperPanel);
    }

    /**
     * Prepare and add the dummy panel in the conrner
     * @param gridBagConstraints
     */
    private void prepareAndAddMockPanel(final GridBagConstraints gridBagConstraints) {
        /*
        *   This is only mock panel - created just in order
        *   fo fill empty space between two NumberPanels
        * */
        UpperNumberPanel mockPanel=new UpperNumberPanel();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        add(mockPanel, gridBagConstraints);
    }

}
