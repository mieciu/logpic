package pl.hejman.logpic.view;


import pl.hejman.logpic.model.FieldState;
import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.model.UpdateAction;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

/**
 *  This class represents panels with numbers on the left side of picture
 */
public class SideNumberPanel extends JPanel implements Observer {

    private Field[][] field;
    private JPanel[][] fieldGroup;

    public SideNumberPanel() {
            super(new GridLayout(3,3));
            fieldGroup = new JPanel[3][3];
            for(int y=0 ; y < 3 ; y++){
                for(int x=0; x< 3 ; x++){
                    fieldGroup[y][x] = new JPanel(new GridLayout(3, 3));
                    fieldGroup[y][x].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                    add(fieldGroup[y][x]);
                }
            }
            field = new Field[9][9];
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    field[y][x] = new Field(x, y);
                    field[y][x].setBackground(Color.LIGHT_GRAY);
                    fieldGroup[y / 3][x / 3].add(field[y][x]);
                }
            }
    }

    /**
     * Set the game with new numbers
     *
     * @param logpicModel   game model
     */
    public void setGame(final LogpicModel logpicModel) {
        int[][] numbers;
        numbers = setNumbersInRows(logpicModel);
        clearNumberPanel(logpicModel);
        for (int y = 0; y < logpicModel.getAmountOfColumns(); y++) {
            for (int x = 0; x < logpicModel.getAmountOfRows(); x++) {
                writeNumberToField(x,y,numbers[x][y]);
            }
        }
    }

    /**
     *  Set numbers in rows of the panel
     * @param logpicModel   game model
     * @return list of numbers to be set
     */
    public int[][] setNumbersInRows(final LogpicModel logpicModel){
        int[][] numbers = new int[logpicModel.getAmountOfColumns()][logpicModel.getAmountOfRows()];
        LinkedList<Integer> rowNumbers = new LinkedList<Integer>();
        for(int i=0 ; i < logpicModel.getAmountOfRows(); i++) {
            rowNumbers.clear();
            int counterOfBlock = 0;
            boolean iAmInBlock = false;
            for(int j = 0; j < logpicModel.getAmountOfColumns(); j++)
            {
                if (logpicModel.getSolutionState(i, j) == FieldState.PAINTED )
                {
                    iAmInBlock=true;
                    counterOfBlock++;
                    if(j==(logpicModel.getAmountOfRows()-1)) {
                        rowNumbers.add(counterOfBlock);
                    }
                }
                else if (logpicModel.getSolutionState(i,j) == FieldState.EMPTY)
                {
                    if(iAmInBlock) {
                        rowNumbers.add(counterOfBlock);
                        counterOfBlock=0;
                    }
                    iAmInBlock=false;
                }
            }
            int iterator=0;
            while(!rowNumbers.isEmpty()){
                numbers[i][iterator]=rowNumbers.pop();
                iterator++;
            }
        }
       return numbers;
    }

    /**
     * Write number to field of the NumberPanel
     * @param x     position of number (coordinate X)
     * @param y     position of number (coordinate Y)
     * @param numberToBeWritten number to be displayed in the field
     */
    public void writeNumberToField(final int x, final int y, final int numberToBeWritten){
        if(numberToBeWritten != 0) {
            this.field[x][y].setText(String.valueOf(numberToBeWritten));
        }
    }

    /**
     * Clear panel from previous games' numbers
     * @param logpicModel game model
     */
    public void clearNumberPanel(final LogpicModel logpicModel) {
        for(int i=0;i< logpicModel.getAmountOfColumns();i++) {
            for(int j=0;j< logpicModel.getAmountOfRows();j++) {
                this.field[i][j].setText("");
            }
        }
    }

    /**
     * React on updates
     * @param o
     * @param arg
     */
    public void update(final Observable o, final Object arg) {
        switch ((UpdateAction)arg) {
            case NEW_GAME:
                setGame((LogpicModel)o);
                break;
            case CHECK:
            case SELECTED_STATE:
        }
    }

}




