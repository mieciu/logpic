package pl.hejman.logpic.view;

import pl.hejman.logpic.controller.ButtonPanelController;
import pl.hejman.logpic.model.UpdateAction;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;


/**
 *  This class draws the button panel and reacts to updates from the model.
 */
public class ButtonPanel extends JPanel implements Observer {

    /* Buttons to control the main game process */
    private JButton buttonNew;
    private JButton buttonCheck;
    private JButton buttonHelp;
    private JButton buttonExit;

    /* Group of buttons to pick a field marker */
    private ButtonGroup buttonGroupWithFieldStates;

    /* Toggle buttons to choose states to paint with */
    private JToggleButton[] buttonsOfFieldStates;

    /* Constant strings to define the action JToggleButtons */
    private final String aNew = "New";
    private final String check = "Check";
    private final String help = "Help";
    private final String exit = "Exit";
    private final String paint = "Paint";
    private final String mark = "Mark";
    private final String clear = "Clear";

    /**
    *   Constructs the panel and arranges all components.
    */
    public ButtonPanel() {
        super(new BorderLayout());
        JPanel pnlAlign = new JPanel();
        pnlAlign.setLayout(new BoxLayout(pnlAlign, BoxLayout.PAGE_AXIS));
        add(pnlAlign, BorderLayout.NORTH);
        JPanel pnlOptions = new JPanel(new FlowLayout(FlowLayout.LEADING));
        pnlOptions.setBorder(BorderFactory.createTitledBorder(" Options "));
        pnlAlign.add(pnlOptions);
        AddComponentsToOptionsPanel(pnlOptions);
        JPanel panelWithFieldStates = new JPanel();
        panelWithFieldStates.setLayout(new BoxLayout(panelWithFieldStates, BoxLayout.PAGE_AXIS));
        panelWithFieldStates.setBorder(BorderFactory.createTitledBorder(" Fields "));
        pnlAlign.add(panelWithFieldStates);
        JPanel pnlNumbersHelp = new JPanel(new FlowLayout(FlowLayout.LEADING));
        panelWithFieldStates.add(pnlNumbersHelp);
        JPanel pnlWithStates = new JPanel(new FlowLayout(FlowLayout.LEADING));
        panelWithFieldStates.add(pnlWithStates);
        initializeButtonsOfFieldStates();
        addButtonsOfFieldstatesToPanel(pnlWithStates);
    }

    /**
     * This method adds buttons with fieldstates to the panel
     * @param pnlWithStates
     */
    private void addButtonsOfFieldstatesToPanel(JPanel pnlWithStates) {
        for( JToggleButton button : buttonsOfFieldStates) {
            button.setFocusable(false);
            buttonGroupWithFieldStates.add(button);
            pnlWithStates.add(button);
        }
    }

    /**
     * This method creates buttons with fieldstates to choose
     */
    private void initializeButtonsOfFieldStates() {
        buttonGroupWithFieldStates = new ButtonGroup();
        buttonsOfFieldStates = new JToggleButton[3];
        buttonsOfFieldStates[0] = new JToggleButton(paint);
        buttonsOfFieldStates[1] = new JToggleButton(mark);
        buttonsOfFieldStates[2] = new JToggleButton(clear);
    }

    /**
     * This method adds Jbuttons with main game options
     * @param panelWithOptions
     */
    private void AddComponentsToOptionsPanel(JPanel panelWithOptions) {
        buttonNew = new JButton(aNew);
        buttonNew.setFocusable(false);
        panelWithOptions.add(buttonNew);
        buttonCheck = new JButton(check);
        buttonCheck.setFocusable(false);
        panelWithOptions.add(buttonCheck);
        buttonHelp = new JButton(help);
        buttonHelp.setFocusable(false);
        panelWithOptions.add(buttonHelp);
        buttonExit = new JButton(exit);
        buttonExit.setFocusable(false);
        panelWithOptions.add(buttonExit);
    }

    /**
     * Update the appearance of the buttons when Model sends update notification.
     * @param o
     * @param arg
     */
    public void update(final Observable o, final Object arg) {
        switch ((UpdateAction)arg) {
            case NEW_GAME:
            case CHECK:
                buttonGroupWithFieldStates.clearSelection();
                break;
            case SELECTED_STATE:
        }
    }

    /**
     * Add controller to all the buttons
     * @param buttonPanelController
     */
    public void addControllerToTheButtons(final ButtonPanelController buttonPanelController) {
        buttonNew.addActionListener(buttonPanelController);
        buttonCheck.addActionListener(buttonPanelController);
        buttonHelp.addActionListener(buttonPanelController);
        buttonExit.addActionListener(buttonPanelController);
        for( JToggleButton button : buttonsOfFieldStates) {
            button.addActionListener(buttonPanelController);
        }
    }

}
