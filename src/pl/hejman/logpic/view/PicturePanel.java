package pl.hejman.logpic.view;

import pl.hejman.logpic.controller.PicturePanelController;
import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.model.UpdateAction;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 *  This class draws the picture panel and reacts to updates from the model.
 */
public class PicturePanel extends JPanel implements Observer {

    private Field[][] field;
    private JPanel[][] fieldGroup;


    public PicturePanel() {
        super(new GridLayout(3,3));
        fieldGroup = new JPanel[3][3];
        for(int y=0 ; y < 3 ; y++){
            for(int x=0; x< 3 ; x++){
                fieldGroup[y][x] = new JPanel(new GridLayout(3, 3));
                fieldGroup[y][x].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                add(fieldGroup[y][x]);
            }
        }
        field = new Field[9][9];
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                field[y][x] = new Field(x, y);
                fieldGroup[y / 3][x / 3].add(field[y][x]);
            }
        }
    }

    /**
     * Set controller on the picture
     * @param picturePanelController
     */
    public void setController(final PicturePanelController picturePanelController) {
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                fieldGroup[y][x].addMouseListener(picturePanelController);
            }
        }
    }

    /**
     * Set the game
     * @param logpicModel
     */
    public void setGame(final LogpicModel logpicModel) {
        for (int y = 0; y < logpicModel.getAmountOfRows(); y++) {
            for (int x = 0; x < logpicModel.getAmountOfColumns(); x++){
                field[y][x].setBackground(Color.WHITE);
                field[y][x].setText("");
            }
        }
    }

    /**
     * This method sets the game with after-checking notifications
     * @param logpicModel
     */
    private void setGameCheck(final LogpicModel logpicModel) {
        for (int y = 0; y < logpicModel.getAmountOfRows(); y++) {
            for (int x = 0; x < logpicModel.getAmountOfColumns(); x++) {
                    field[x][y].setBackground(logpicModel.isProperlySetByUserAtField(x, y) ? Color.GREEN : Color.RED);
            }
        }
    }

    /**
     * This method removes after-checking notifications
     * @param logpicModel
     */
    private void unsetGameCheck(final LogpicModel logpicModel) {
        for (int x = 0; x < logpicModel.getAmountOfRows(); x++) {
            for (int y = 0; y < logpicModel.getAmountOfColumns(); y++) {
                field[x][y].setAppearanceAccordingToState(logpicModel.getFieldState(x, y));
            }
        }
    }

    /**
     * This method reacts on the update notifications
     * @param o
     * @param arg
     */
    public void update(final Observable o, final Object arg) {
        switch ((UpdateAction)arg) {
            case NEW_GAME:
                setGame((LogpicModel)o);
                break;
            case CHECK:
                setGameCheck((LogpicModel)o);
                break;
            case SELECTED_STATE:
                unsetGameCheck((LogpicModel)o);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

}
