package pl.hejman.logpic.view;

import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.model.UpdateAction;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 *  This class represents the pop-up window displayed after player decided to verify
 *  his game. Its content depends of the game result.
 */
public class PopUpAfterChecking extends JFrame implements Observer{

    private String text;
    private Icon icon;
    private final String gameWonString = "Congratulations!";
    private final String gameLostString = "Sorry, please correct your mistakes...";
    private final String gameWonPicturePath = "media/smileface.png";
    private final String gameLostPicturePath = "media/sadface.png";

    /**
     * React on pressing the proper button
     * @param o
     * @param arg
     */
    public void update(final Observable o, final Object arg) {
        switch ((UpdateAction)arg) {
            case CHECK:
                displaySuitablePicture(o);
                break;
            case SELECTED_STATE:
                break;
            case NEW_GAME:
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Display picture depending on game result
     * @param o
     */
    public void displaySuitablePicture(final Observable o) {
        setText((LogpicModel) o);
        setIcon((LogpicModel) o);
        JOptionPane.showMessageDialog(null, getText(), null, JOptionPane.INFORMATION_MESSAGE, getIcon());
    }

    /**
     * @return  text from picture
     */
    public String getText() {
        return text;
    }

    /**
     * Set proper text of picture to be displayed
     * @param logpicModel game model
     */
    private void setText(final LogpicModel logpicModel) {
        if(logpicModel.isCurrentGameWon()) {
            text=gameWonString;
        }
        else {
            text=gameLostString;
        }
    }

    /**
     * @return  icon form the picture
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * Set the proper text of picture to be displayed
     * @param logpicModel    game model
     */
    private void setIcon(final LogpicModel logpicModel) {
        if(logpicModel.isCurrentGameWon()) {
            icon = new ImageIcon(gameWonPicturePath);
        }
        else {
            icon = new ImageIcon(gameLostPicturePath);
        }
    }

}
