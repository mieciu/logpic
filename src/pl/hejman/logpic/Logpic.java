package pl.hejman.logpic;

import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.view.MainLogpicPanel;

import javax.swing.*;

/* The main class of the program */
public class Logpic {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            LogpicModel model = new LogpicModel();
            MainLogpicPanel view  = new MainLogpicPanel(model);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

}