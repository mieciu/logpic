package pl.hejman.logpic.controller;

import pl.hejman.logpic.model.FieldState;
import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.model.UpdateAction;
import pl.hejman.logpic.view.PopUpWithHelp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** This is the controller of the panel with buttons */
public class ButtonPanelController implements ActionListener {

    private final LogpicModel logpicModel;
    private final String aNew = "New";
    private final String check = "Check";
    private final String help = "Help";
    private final String exit = "Exit";
    private final String paint = "Paint";
    private final String mark = "Mark";
    private final String clear = "Clear";

    public ButtonPanelController(LogpicModel logpicModel) {
        this.logpicModel = logpicModel;
    }

    /** Displays pop-up window with help message */
    public void displayPopUpWithHelp() {
        PopUpWithHelp popUpWithHelp = new PopUpWithHelp();
        popUpWithHelp.display();
    }

    /**
     * Perform actions after pressing certain button
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(aNew))
            informToCreateNewGame();
        else if (e.getActionCommand().equals(check))
            informToCheckGame();
        else if (e.getActionCommand().equals(help))
            displayPopUpWithHelp();
        else if (e.getActionCommand().equals(exit))
            System.exit(0);
        else if(e.getActionCommand().equals(paint))
            userChosenFieldState(FieldState.PAINTED);
        else if(e.getActionCommand().equals(mark))
            userChosenFieldState(FieldState.MARKED);
        else if(e.getActionCommand().equals(clear))
            userChosenFieldState(FieldState.EMPTY);
        else
            throw new IllegalArgumentException(e.getActionCommand());
    }

    /**
     * Notify that user has chosen a fieldstate to paint with
     * @param fieldState
     */
    private void userChosenFieldState(final FieldState fieldState) {
        logpicModel.setSelectedState(fieldState);
        logpicModel.setChanged();
        logpicModel.notifyObservers(UpdateAction.SELECTED_STATE);
    }

    /**
     * Notify that the game needs to be check
     */
    private void informToCheckGame() {
        logpicModel.setChanged();
        logpicModel.notifyObservers(UpdateAction.CHECK);
    }

    /**
     * Notify that new game needs to be created
     */
    private void informToCreateNewGame() {
        logpicModel.createNewGame();
        logpicModel.setChanged();
        logpicModel.notifyObservers(UpdateAction.NEW_GAME);
    }

}
