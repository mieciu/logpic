package pl.hejman.logpic.controller;

import pl.hejman.logpic.model.FieldState;
import pl.hejman.logpic.model.LogpicModel;
import pl.hejman.logpic.view.Field;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *  This is the controller of PicturePanel - it reacts for player's drawing with mouse pointer
 */
public class PicturePanelController implements MouseListener {

    private LogpicModel logpicModel;

    public PicturePanelController(LogpicModel logpicModel) {
        this.logpicModel = logpicModel;
    }

    /**
     * This method implements reactions after pressing the mouse button on the drawn picture
     * @param e
     */
    public void mousePressed(MouseEvent e) {
        JPanel panel = (JPanel)e.getSource();
        Component component = panel.getComponentAt(e.getPoint());
        if (component instanceof Field) {
            Field field = (Field)component;
            if (e.getButton() == MouseEvent.BUTTON1 ) {
                setFieldAfterPressingMouseButton(field, field.getCoordinateX(), field.getCoordinateY());
            }
        }
    }

    /**
     * This method is to set a single field appearance according to selected by player state
     * @param field which appeararance needs to be changed
     * @param x  coordinate X of the field
     * @param y  coordinate Y of the field
     */
    private void setFieldAfterPressingMouseButton(final Field field, final int x, final int y) {
        FieldState selectedState = logpicModel.getSelectedState();
        logpicModel.setFieldState(y, x, selectedState);
        field.setAppearanceAccordingToState(selectedState);
    }

    public void mouseClicked(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }

}
